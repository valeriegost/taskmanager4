package ru.volnenko.tm.entity;

import java.io.Serializable;
import java.util.List;

public class Domain implements Serializable {

    private List<Project> projects;
    private List<Task> tasks;

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

}
